﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Direction {

    public Vector3 Vector {
        get; set;
    }

    public Direction(Vector3 vector) {
        this.Vector = vector;
    }
}

public static class Directions {
    public static readonly Direction NORTH = new Direction(Vector3.up);
    public static readonly Direction SOUTH = new Direction(Vector3.down);
    public static readonly Direction EAST = new Direction(Vector3.right);
    public static readonly Direction WEST = new Direction(Vector3.left);
}
