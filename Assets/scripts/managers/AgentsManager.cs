﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentsManager : MonoBehaviour {

    private GameBoard gameBoard;

    [SerializeField]
    private GameObject bombermanPrefab;

    private List<Agent> agents;

	void Start() {
        this.gameBoard = GameObject.Find("/Level1").GetComponent<GameBoard>();

        this.agents = new List<Agent>();

        GameObject bomberman = Instantiate(bombermanPrefab);
        bomberman.transform.position = this.gameBoard.TileIndexToWorldPosition(1, 1);
        this.agents.Add(new Player(bomberman.GetComponent<Bomberman>()));
		this.gameBoard.AddPlayer(bomberman);
	}
}
