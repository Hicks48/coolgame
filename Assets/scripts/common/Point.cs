﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Point {

    public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

    public int x;
    public int y;
}
