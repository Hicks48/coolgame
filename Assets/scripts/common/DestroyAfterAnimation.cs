﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterAnimation : MonoBehaviour {

	public void Start () {
		float delay = GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
		Destroy(gameObject, delay);
	}

}
