﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "tile", menuName = "TileDefinition", order = 1)]
public class TileDefinition : ScriptableObject {
	public bool walkable = false;
	public bool destructible = false;
	public Sprite sprite;
	public GameObject destructionPrefab;
}
