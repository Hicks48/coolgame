﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "powerUpDefinition", menuName = "PowerUpDefinition", order = 1)]
public class PowerUpDefinition : ScriptableObject {
	public GameObject powerUpPrefab;
	public float probability;
}
