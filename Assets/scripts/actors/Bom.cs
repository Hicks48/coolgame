﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bom : MonoBehaviour {

    [SerializeField]
    private float fuseTime;

    [SerializeField]
    private float tickingMaxScale;

    [SerializeField]
    private float tickingInterval;

	[SerializeField]
	private float movementSpeed;

    private float currentTickingTime;

    private Vector3 originalScale;

	private Point tilePosition;
	private GameBoard gameBoard;
    private GameObject bombOwner;
	private bool isMoving;
	private Point movementDirection;
	private float subPosition; //current position between last tile center and current tile center

	public void Start() {
		this.gameBoard = GameObject.FindWithTag("GameBoard").GetComponent<GameBoard>();

        this.currentTickingTime = 0.0f;
        this.originalScale = this.gameObject.transform.localScale;
		this.isMoving = false;
    }

	public void Update() {
        this.fuseTime -= Time.deltaTime;

		if(this.fuseTime < 0.0f || gameBoard.IsDeadly(tilePosition)) {
			Explode();
        }

        this.Tick();

		if(this.isMoving) Move(this.movementSpeed * Time.deltaTime);
		Vector3 direction = new Vector3(movementDirection.x, movementDirection.y, 0f);
		transform.position = gameBoard.TileIndexToWorldPosition(tilePosition) + direction * subPosition;
	}

    public void SetBombOwner(GameObject bombOwner) {
        this.bombOwner = bombOwner;
    }

	public void SetTilePosition(Point tilePosition) {
		this.tilePosition = tilePosition;
	}

	public void Kick(Point direction) {
		this.isMoving = true;
		this.movementDirection = direction;
		this.subPosition = 0f;
	}

    private void Tick() {
        // Update ticking time
        this.currentTickingTime += Time.deltaTime;

        // Reset ticking time id necessary
        if (this.currentTickingTime > this.tickingInterval) {
            this.currentTickingTime = 0.0f;
        }

        // Scaling to max scale
        if (this.currentTickingTime <= (this.tickingInterval / 2.0f)) {
            float currentTickingPercentage = this.currentTickingTime / (this.tickingInterval / 2.0f);
            this.gameObject.transform.localScale = new Vector3(Mathf.Lerp(this.originalScale.x, this.tickingMaxScale, currentTickingPercentage), Mathf.Lerp(this.originalScale.y, this.tickingMaxScale, currentTickingPercentage), 1.0f);
        }
        
        // Back to original scale
        else {
            float currentTickingPercentage = (this.currentTickingTime - (this.tickingInterval / 2.0f)) / (this.tickingInterval / 2.0f);
            this.gameObject.transform.localScale = new Vector3(Mathf.Lerp(this.tickingMaxScale, this.originalScale.x, currentTickingPercentage), Mathf.Lerp(this.tickingMaxScale, this.originalScale.y, currentTickingPercentage), 1.0f);
        }
    }

	//recursively move tile by tile until all delta has been consumed
	private void Move(float delta) {
		if(delta <= 0f) return;
		Point nextTile = new Point(tilePosition.x + movementDirection.x, tilePosition.y + movementDirection.y);
		this.subPosition += delta;
		if(this.subPosition >= 0f && !this.gameBoard.IsEmpty(nextTile))
			StopMovement();
		else if(this.subPosition >= 0f) {
			MoveToNewTile(nextTile);
			delta = this.subPosition;
			this.subPosition = -1f;
			Move(delta);
		}
	}

	private void MoveToNewTile(Point newTile) {
		gameBoard.MoveBomb(tilePosition, newTile);
		tilePosition = newTile;
	}

	private void StopMovement() {
		this.isMoving = false;
		this.subPosition = 0f;
	}

	private void Explode() {
        this.bombOwner.SendMessage("BomDestroyed");
		gameBoard.AddExplosion(tilePosition, bombOwner.GetComponent<Bomberman>().GetBombExplosionSize());
		gameBoard.RemoveBomb(tilePosition);
		Destroy(gameObject);
	}
}
