﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public abstract class Actor : NetworkBehaviour {

    protected Agent controller;

    public void RegisterController(Agent controller) {
        this.controller = controller;
    }

    protected void UpdateController() {

        if (this.controller != null && this.isLocalPlayer) {
            this.controller.Update();
        }
    }
}
