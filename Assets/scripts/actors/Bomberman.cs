﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class BombermanAnimationHandles {
    public const string WALKING_NORTH_ANIMATION_HANDLE = "walking_north";
    public const string WALKING_SOUTH_ANIMATION_HANDLE = "walking_south";
    public const string WALKING_EAST_ANIMATION_HANDLE = "walking_east";
    public const string WALKING_WEST_ANIMATION_HANDLE = "walking_west";

    public const string IDLE_NORTH_ANIMATION_HANDLE = "idle_north";
    public const string IDLE_SOUTH_ANIMATION_HANDLE = "idle_south";
    public const string IDLE_EAST_ANIMATION_HANDLE = "idle_east";
    public const string IDLE_WEST_ANIMATION_HANDLE = "idle_west";
}


public class Bomberman : Actor {

    [SerializeField]
    private GameObject bom;

    [SerializeField]
    private int maxBoms;

    private int currentBoms;

    [SerializeField]
    private float speed;

	[SerializeField]
	private int bombExplosionSize;

    private GameBoard gameBoard;

    private Animator animator;

    private Vector3 velocity;
    private Direction direction;

    private bool canKickBoms;

	public void Start() {
        // TODO: Remove only temporary
        this.controller = new Player(this);

        this.gameBoard = GameObject.Find("/Level1").GetComponent<GameBoard>();

        this.animator = this.GetComponent<Animator>();

        this.currentBoms = 0;

        this.direction = Directions.SOUTH;
        this.velocity = Vector3.zero;
    }
	
	public void Update() {
        this.InitializeUpdate();
        this.UpdateController();
        this.UpdateAnimation();
	}

    public void SetCanKickBoms(bool canKickBoms) {
        this.canKickBoms = canKickBoms;
    }

    public void SetSpeed(float speed) {
        this.speed = speed;
    }

	public void IncreaseSpeed() {
		this.speed += 0.5f;
	}

    public void SetMaxBombs(int maxBoms) {
        this.maxBoms = maxBoms;
    }

	public void IncreaseMaxBombs() {
		this.maxBoms++;
	}

	public int GetBombExplosionSize() {
		return bombExplosionSize;
	}

	public void IncreaseBombExplosionSize() {
		this.bombExplosionSize++;
	}

    public void DropBom() {

        if (this.currentBoms < this.maxBoms) {
            bool success = this.gameBoard.AddBomb(this.GetCurrentTile(), this.gameObject);

            if (success) {
                this.currentBoms++;
            }
        }
    }

    public void BomDestroyed() {
        this.currentBoms--;
    }

    public void MoveNorth() {
        this.Move(BombermanAnimationHandles.WALKING_NORTH_ANIMATION_HANDLE, Directions.NORTH);
    }

    public void MoveSouth() {
        this.Move(BombermanAnimationHandles.WALKING_SOUTH_ANIMATION_HANDLE, Directions.SOUTH);
    }

    public void MoveEast() {
        this.Move(BombermanAnimationHandles.WALKING_EAST_ANIMATION_HANDLE, Directions.EAST);
    }

    public void MoveWest() {
        this.Move(BombermanAnimationHandles.WALKING_WEST_ANIMATION_HANDLE, Directions.WEST);
    }

	public void Die() {
		Debug.Log("TODO: player death");
	}

    private void Move(string animationHandle, Direction direction) {

        this.direction = direction;

        Point targetTile = this.GetMovementTargetTile(direction);

        // Check bom kicking
        if (this.canKickBoms && this.gameBoard.IsBomb(targetTile)) {
            this.gameBoard.KickBomb(targetTile, new Point(Mathf.RoundToInt(this.direction.Vector.x), Mathf.RoundToInt(this.direction.Vector.y)));
        }

        if (this.IsNotAllowedToMoveToTargetTile(targetTile)) {
            return;
        }

        this.MoveBomberman(targetTile, animationHandle);
    }

    private Point GetMovementTargetTile(Direction direction) {
        Point currentTile = this.GetCurrentTile();
        Point nextNonCurrentTileInDirection = new Point(Mathf.RoundToInt(currentTile.x + direction.Vector.x), Mathf.RoundToInt(currentTile.y + direction.Vector.y));

        float currentX = this.transform.position.x;
        float currentY = this.transform.position.y;

        Vector3 currentTilePosition = this.TileIndexToWorldPositionCenter((currentTile));
        Vector2 fromCurrentPositionToCurrentTile = new Vector2(currentTilePosition.x - currentX, currentTilePosition.y - currentY);

        Vector3 nextNonCurrentTileInDirectionPosition = this.TileIndexToWorldPositionCenter(nextNonCurrentTileInDirection);
        Vector2 fromCurrentPositionToNextNonCurrentTileInDirection = new Vector2(nextNonCurrentTileInDirectionPosition.x - currentX, nextNonCurrentTileInDirectionPosition.y - currentY);

        // Current tile center is in front
        if (!this.AreNearPerpendicular(new Vector2(direction.Vector.x, direction.Vector.y), fromCurrentPositionToCurrentTile)
                && this.Are2DParalelVectorsPointingSameDirection(fromCurrentPositionToNextNonCurrentTileInDirection, fromCurrentPositionToCurrentTile)
                && fromCurrentPositionToCurrentTile.magnitude > 0.000000001f) {

            return currentTile;
        }

        // Current tile center is behind
        else {

            if (this.gameBoard.IsWalkable(nextNonCurrentTileInDirection)) {
                return nextNonCurrentTileInDirection;
            }

            return this.LookForAlternativeTargetTile(nextNonCurrentTileInDirection, direction);
        }
    }

    private Point LookForAlternativeTargetTile(Point currentTarget, Direction direction) {
        Point currentTile = this.GetCurrentTile();
        Vector3 fromCurrentTileCenterToCurrentPosition = this.transform.position - this.TileIndexToWorldPositionCenter(currentTile);

        // If is now on the edge of the tile or is not near perpendicular to direction can't find alternative tile
        if (fromCurrentTileCenterToCurrentPosition.magnitude < 0.3 * this.gameBoard.tileWidth 
                || !this.AreNearPerpendicular(new Vector2(direction.Vector.x, direction.Vector.y), new Vector2(fromCurrentTileCenterToCurrentPosition.x, fromCurrentTileCenterToCurrentPosition.y))) {
            return currentTarget;
        }

        // Check if possible future destination is walkable
        fromCurrentTileCenterToCurrentPosition.Normalize();
        Point futureDestination = new Point(Mathf.RoundToInt(currentTile.x + fromCurrentTileCenterToCurrentPosition.x + direction.Vector.x), Mathf.RoundToInt(currentTile.y + fromCurrentTileCenterToCurrentPosition.y + direction.Vector.y));
        if (!this.gameBoard.IsWalkable(futureDestination)) {
            return currentTarget;
        }

        return new Point(Mathf.RoundToInt(currentTile.x + fromCurrentTileCenterToCurrentPosition.x), Mathf.RoundToInt(currentTile.y + fromCurrentTileCenterToCurrentPosition.y));
    }

    private Vector3 TileIndexToWorldPositionCenter(Point titleIndex) {
        return this.gameBoard.TileIndexToWorldPosition(titleIndex);
    }

    private bool AreNearPerpendicular(Vector2 a, Vector2 b) {
        return Mathf.Abs(Vector2.Dot(a, b)) < 0.01;
    }

    private bool AreParralel(Vector2 a, Vector2 b) {
        // If one of the components is zero for both are parralel. Since all one dimmensional vectors are parralel
        if (Mathf.Abs(a.x) < 0.1 || Mathf.Abs(b.x) < 0.1) {
            return Mathf.Abs(a.x) < 0.1 && Mathf.Abs(b.x) < 0.1;
        }

        if (Mathf.Abs(a.y) < 0.1 || Mathf.Abs(b.y) < 0.1) {
            return Mathf.Abs(a.y) < 0.1 && Mathf.Abs(b.y) < 0.1;
        }

        return Mathf.Abs(b.x / a.x - b.y / a.y) < 0.01;
    }

    private bool Are2DParalelVectorsPointingSameDirection(Vector2 a, Vector2 b) {
        return (b.x + b.y) / (a.x + a.y) >= 0;
    }

    private bool IsNotAllowedToMoveToTargetTile(Point targetTile) {
        return !targetTile.Equals(this.GetCurrentTile()) && !this.gameBoard.IsWalkable(targetTile);
    }

    private void MoveBomberman(Point targetTile, string animationHandle) {
        float currentX = this.transform.position.x;
        float currentY = this.transform.position.y;

        Vector3 targetTilePosition = this.TileIndexToWorldPositionCenter(targetTile);

        Vector3 velocityDirection = new Vector3(targetTilePosition.x - currentX, targetTilePosition.y - currentY);
        velocityDirection.Normalize();

        this.velocity = velocityDirection * this.speed * Time.deltaTime;
        this.gameObject.transform.Translate(this.velocity);

        this.animator.Play(animationHandle);
    }

    private void InitializeUpdate() {
        this.velocity = Vector3.zero;
    }

    private void UpdateAnimation() {

        if (this.velocity.Equals(Vector3.zero)) {
            this.DisplayIdleAnimation();
        }
    }

    private void DisplayIdleAnimation() {

        if (this.direction.Equals(Directions.NORTH)) {
            this.animator.Play(BombermanAnimationHandles.IDLE_NORTH_ANIMATION_HANDLE);
        } else if (this.direction.Equals(Directions.SOUTH)) {
            this.animator.Play(BombermanAnimationHandles.IDLE_SOUTH_ANIMATION_HANDLE);
        } else if (this.direction.Equals(Directions.EAST)) {
            this.animator.Play(BombermanAnimationHandles.IDLE_EAST_ANIMATION_HANDLE);
        } else if (this.direction.Equals(Directions.WEST)) {
            this.animator.Play(BombermanAnimationHandles.IDLE_WEST_ANIMATION_HANDLE);
        }
    }

    private bool IsCurrentTileWalkable() {
        return this.gameBoard.IsWalkable(this.GetCurrentTile());
    }

    private Point GetCurrentTile() {
        return this.gameBoard.WorldPositionToTileIndex(this.gameObject.transform.position);
    }
}
