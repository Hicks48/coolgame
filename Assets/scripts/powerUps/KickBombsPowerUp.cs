﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KickBombsPowerUp : MonoBehaviour {

	public void ApplyToPlayer(GameObject player) {
		player.GetComponent<Bomberman>().SetCanKickBoms(true);
	}
}
