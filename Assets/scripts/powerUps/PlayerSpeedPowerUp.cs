﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpeedPowerUp : MonoBehaviour {

	public void ApplyToPlayer(GameObject player) {
		player.GetComponent<Bomberman>().IncreaseSpeed();
	}
}
