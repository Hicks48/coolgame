﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombSizePowerUp : MonoBehaviour {

	public void ApplyToPlayer(GameObject player) {
		player.GetComponent<Bomberman>().IncreaseBombExplosionSize();
	}
}
