﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoreBombsPowerUp : MonoBehaviour {

	public void ApplyToPlayer(GameObject player) {
		player.GetComponent<Bomberman>().IncreaseMaxBombs();
	}
}
