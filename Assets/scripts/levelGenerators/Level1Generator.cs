﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1Generator : MonoBehaviour {

	public TileDefinition floor;
	public TileDefinition wall;
	public TileDefinition brickWall;

	private GameBoard gameBoard;

	public void Start() {
		gameBoard = GetComponent<GameBoard>();
		GenerateLevel();
	}

	private void GenerateLevel() {
		int width = gameBoard.width;
		int height = gameBoard.height;
		gameBoard.BoxFillTiles(new Point(0, 0), new Point(width + 1, height + 1), floor);
		//edge walls
		gameBoard.BoxFillTiles(new Point(0, 0), new Point(width, 1), wall); //bottom
		gameBoard.BoxFillTiles(new Point(0, 0), new Point(1, height), wall); //left
		gameBoard.BoxFillTiles(new Point(width - 1, 0), new Point(width, height), wall); //right
		gameBoard.BoxFillTiles(new Point(0, height - 1), new Point(width, height), wall); //top
		//random brickwall
		for(int x = 1; x < (width - 1); x++) {
			for(int y = 1; y < (height - 1); y++) {
				if(Random.value < 0.9f) gameBoard.SetTile(new Point(x, y), brickWall);
			}
		}
		//player start corners
		gameBoard.BoxFillTiles(new Point(1, 1), new Point(4, 2), floor); //bottom left
		gameBoard.BoxFillTiles(new Point(1, 1), new Point(2, 4), floor); //bottom left
		gameBoard.BoxFillTiles(new Point(width - 4, 1), new Point(width - 1, 2), floor); //bottom right
		gameBoard.BoxFillTiles(new Point(width - 2, 1), new Point(width - 1, 4), floor); //bottom right
		gameBoard.BoxFillTiles(new Point(1, height - 2), new Point(4, height - 1), floor); //top left
		gameBoard.BoxFillTiles(new Point(1, height - 4), new Point(2, height - 1), floor); //top left
		gameBoard.BoxFillTiles(new Point(width - 4, height - 2), new Point(width - 1, height - 1), floor); //top right
		gameBoard.BoxFillTiles(new Point(width - 2, height - 4), new Point(width - 1, height - 1), floor); //top right
		//that wall pattern thing
		for(int x = 0; x < (width - 1); x += 2) {
			for(int y = 0; y < (height - 1); y += 2) {
				gameBoard.SetTile(new Point(x, y), wall);
			}
		}
	}

}
