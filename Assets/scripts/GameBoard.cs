﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBoard : MonoBehaviour {

	public int width;
	public int height;
	public float tileWidth;
	public float tileHeight;
	public TileDefinition defaultTile;
	public GameObject templateTile;
	public PowerUpDefinition[] powerUpDefinitions;
	public GameObject bombPrefab;
	public GameObject explosionCenterPrefab;
	public GameObject explosionHorizontalPrefab;
	public GameObject explosionVerticalPrefab;
	public GameObject explosionTopPrefab;
	public GameObject explosionBottomPrefab;
	public GameObject explosionLeftPrefab;
	public GameObject explosionRightPrefab;

	private TileDefinition[,] tiles;
	private SpriteRenderer[,] tileRenderers;
	private GameObject tileInstanceRoot;
	private float[,] explosions; //array of the last times (Time.time) an explosion is in each tile
	private GameObject[,] bombs;
	private float explosionLength = 0.5f;
	private GameObject[,] powerUps;
	private HashSet<GameObject> players;

	public void Awake() {
		tileInstanceRoot = new GameObject("tileInstances");
		tileInstanceRoot.transform.parent = transform;
		InitTiles();
		InstantiateTileRenderers();
		explosions = new float[width, height];
		bombs = new GameObject[width, height];
		powerUps = new GameObject[width, height];
		players = new HashSet<GameObject>();
	}

	public void Start() {
		Vector3 center = transform.position + Vector3.right * ((width / 2f) * tileWidth) + Vector3.up * ((height / 2f) * tileHeight) + Vector3.back * 3f;
		GameObject camera = GameObject.FindWithTag("MainCamera");
		camera.transform.position = center;
		camera.GetComponent<Camera>().orthographicSize = (height / 2f) * tileHeight;
	}

	public void Update() {
		foreach(GameObject player in players) {
			Point position = WorldPositionToTileIndex(player.transform.position);
			if(IsDeadly(position)) player.SendMessage("Die");
			if(IsPowerUp(position)) {
				powerUps[position.x, position.y].SendMessage("ApplyToPlayer", player);
				DestroyPowerUp(position);
			}
		}
	}

	public Point WorldPositionToTileIndex(Vector3 worldPosition) {
		return new Point((int) (worldPosition.x / tileWidth), (int) (worldPosition.y / tileHeight));
	}

	public Vector3 TileIndexToWorldPosition(Point tileIndex) {
		return TileIndexToWorldPosition(tileIndex.x, tileIndex.y);
	}

	public Vector3 TileIndexToWorldPosition(int x, int y) {
		Vector3 worldPosition = new Vector3();
		worldPosition.x = transform.position.x + x * tileWidth + tileWidth / 2f;
		worldPosition.y = transform.position.y + y * tileHeight + tileHeight / 2f;
		worldPosition.z = transform.position.z;
		return worldPosition;
	}

	//returns true if tile does not contain non-walkable tile, player, bomb or power-up
	public bool IsEmpty(Point tileIndex) {
		bool playerInTile = false;
		foreach(GameObject player in players) {
			if(WorldPositionToTileIndex(player.transform.position).Equals(tileIndex))
				playerInTile = true;
		}
		return !playerInTile && IsWalkable(tileIndex) && powerUps[tileIndex.x, tileIndex.y] == null;
	}

	public bool IsWalkable(Point tileIndex) {
		if(!IsValidTileIndex(tileIndex)) return false;
		return GetTileDefinition(tileIndex).walkable && !IsBomb(tileIndex);
	}

	public bool IsDeadly(Point tileIndex) {
		if(!IsValidTileIndex(tileIndex)) return true;
		return Time.time < explosions[tileIndex.x, tileIndex.y];
	}

	public TileDefinition GetTileDefinition(Point tileIndex) {
		if(!IsValidTileIndex(tileIndex)) return defaultTile;
		return tiles[tileIndex.x, tileIndex.y];
	}

	public void SetTile(Point tileIndex, TileDefinition tileDefinition) {
		if(!IsValidTileIndex(tileIndex)) return;
		tiles[tileIndex.x, tileIndex.y] = tileDefinition;
		tileRenderers[tileIndex.x, tileIndex.y].sprite = tileDefinition.sprite;
	}

	public void BoxFillTiles(Point start, Point end, TileDefinition tileDefinition) {
		for(int x = start.x; x < end.x; x++) {
			for(int y = start.y; y < end.y; y++ ) {
				SetTile(new Point(x, y), tileDefinition);
			}
		}
	}

	public void AddPlayer(GameObject player) {
		players.Add(player);
	}

	public void RemovePlayer(GameObject player) {
		players.Remove(player);
	}

	public bool IsBomb(Point tileIndex) {
		if(!IsValidTileIndex(tileIndex)) return false;
		return bombs[tileIndex.x, tileIndex.y] != null;
	}

	public bool AddBomb(Point position, GameObject bombOwner) {
		if(!IsValidTileIndex(position)) return false;
		if(IsBomb(position)) return false;
		GameObject instance = Instantiate(bombPrefab, TileIndexToWorldPosition(position), Quaternion.identity);
		instance.GetComponent<Bom>().SetTilePosition(position);
        instance.GetComponent<Bom>().SetBombOwner(bombOwner);
        bombs[position.x, position.y] = instance;
		return true;
	}

	public void RemoveBomb(Point position) {
		Destroy(bombs[position.x, position.y]);
		bombs[position.x, position.y] = null;
	}

	public void MoveBomb(Point oldPosition, Point newPosition) {
		bombs[newPosition.x, newPosition.y] = bombs[oldPosition.x, oldPosition.y];
		bombs[oldPosition.x, oldPosition.y] = null;
	}

	public void KickBomb(Point bombPosition, Point direction) {
		bombs[bombPosition.x, bombPosition.y].GetComponent<Bom>().Kick(direction);
	}

	public void AddExplosion(Point center, int size) {
		float explosionEndTime = Time.time + explosionLength;
		explosions[center.x, center.y] = explosionEndTime;
		Instantiate(explosionCenterPrefab, TileIndexToWorldPosition(center) + Vector3.back, Quaternion.identity);
		AddExplosionsFromTowards(center, new Point(0, 1), size, explosionEndTime, explosionVerticalPrefab, explosionTopPrefab); //up
		AddExplosionsFromTowards(center, new Point(0, -1), size, explosionEndTime, explosionVerticalPrefab, explosionBottomPrefab); //down
		AddExplosionsFromTowards(center, new Point(1, 0), size, explosionEndTime, explosionHorizontalPrefab, explosionRightPrefab); //right
		AddExplosionsFromTowards(center, new Point(-1, 0), size, explosionEndTime, explosionHorizontalPrefab, explosionLeftPrefab); //left
	}


	private bool IsValidTileIndex(Point tileIndex) {
		return tileIndex.x >= 0 && tileIndex.y >= 0 && tileIndex.x < width && tileIndex.y < height;
	}

	private void InitTiles() {
		tiles = new TileDefinition[width, height];
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				tiles[i, j] = defaultTile;
			}
		}
	}

	private void InstantiateTileRenderers() {
		tileRenderers = new SpriteRenderer[width, height];
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				GameObject tileInstance = Instantiate(templateTile, TileIndexToWorldPosition(i, j), Quaternion.identity, tileInstanceRoot.transform);
				tileInstance.name = "tile" + i + "_" + j;
				SpriteRenderer tileRenderer = tileInstance.GetComponent<SpriteRenderer>();
				tileRenderer.sprite = defaultTile.sprite;
				tileRenderers[i, j] = tileRenderer;
			}
		}
	}

	private bool IsDestructible(Point tileIndex) {
		if(!IsValidTileIndex(tileIndex)) return false;
		return tiles[tileIndex.x, tileIndex.y].destructible;
	}

	private void DestroyTile(Point tileIndex) {
		if(!IsValidTileIndex(tileIndex)) return;
		Instantiate(tiles[tileIndex.x, tileIndex.y].destructionPrefab, TileIndexToWorldPosition(tileIndex), Quaternion.identity);
		SetTile(tileIndex, defaultTile);
		AddRandomPowerUp(tileIndex);
	}

	private bool IsPowerUp(Point tileIndex) {
		return powerUps[tileIndex.x, tileIndex.y] != null;
	}

	private void DestroyPowerUp(Point tileIndex) {
		Destroy(powerUps[tileIndex.x, tileIndex.y]);
		powerUps[tileIndex.x, tileIndex.y] = null;
		//TODO: powerup destruction animation
	}

	private void AddRandomPowerUp(Point tileIndex) {
		if(!IsValidTileIndex(tileIndex)) return;
		float r = Random.value;
		float sum = 0f;
		for(int i = 0; i < powerUpDefinitions.Length; i++) {
			sum += powerUpDefinitions[i].probability;
			if(r < sum) {
				AddPowerUp(tileIndex, powerUpDefinitions[i]);
				break;
			}
		}
	}

	private void AddPowerUp(Point tileIndex, PowerUpDefinition powerUpDefinition) {
		powerUps[tileIndex.x, tileIndex.y] = Instantiate(powerUpDefinition.powerUpPrefab, TileIndexToWorldPosition(tileIndex), Quaternion.identity);
	}

	//creates a line of explosion from center toward direction
	private void AddExplosionsFromTowards(Point center, Point direction, int size, float explosionEndTime, GameObject explosionMiddle, GameObject explosionEnd) {
		for(int i = 1; i <= size; i++) {
			Point tileIndex = new Point(center.x + direction.x * i, center.y + direction.y * i);
			if(IsDestructible(tileIndex)) {
				DestroyTile(tileIndex);
				break;
			}
			if(!IsWalkable(tileIndex) && !IsBomb(tileIndex)) break;
			if(IsPowerUp(tileIndex)) {
				DestroyPowerUp(tileIndex);
				break;
			}
			explosions[tileIndex.x, tileIndex.y] = explosionEndTime;
			Vector3 explosionAnimationPosition = TileIndexToWorldPosition(tileIndex) + Vector3.back;
			if(i == size) Instantiate(explosionEnd, explosionAnimationPosition, Quaternion.identity);
			else Instantiate(explosionMiddle, explosionAnimationPosition, Quaternion.identity);
			if(IsBomb(tileIndex)) break;
		}
	}
}
