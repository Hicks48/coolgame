﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoveDirection {
    NORTH, SOUTH, EAST, WEST, NONE
}

public class Player : Agent {

    private Bomberman bombermanActor;

    public Player(Bomberman bombermanActor) {
        this.bombermanActor = bombermanActor;
        this.bombermanActor.RegisterController(this);
    }
	
	public override void Update() {
        this.handleMovementInput();
        this.handleActionsInput();
    }

    private void handleActionsInput() {

        if (Input.GetButton("Fire1")) {
            this.bombermanActor.DropBom();
        }
    }

    private void handleMovementInput() {

        switch (this.GetMoveDirection()) {
            case MoveDirection.NORTH:
                this.bombermanActor.MoveNorth();
                break;

            case MoveDirection.SOUTH:
                this.bombermanActor.MoveSouth();
                break;

            case MoveDirection.EAST:
                this.bombermanActor.MoveEast();
                break;

            case MoveDirection.WEST:
                this.bombermanActor.MoveWest();
                break;
        } 
    }

    private MoveDirection GetMoveDirection() {
        float horizontalInputValue = Input.GetAxis("Horizontal");
        float verticalInputValue = Input.GetAxis("Vertical");
        
        if (verticalInputValue > 0.0f) {
            return MoveDirection.NORTH;
        } else if (verticalInputValue < 0.0f) {
            return MoveDirection.SOUTH;
        } else if (horizontalInputValue > 0.0f) {
            return MoveDirection.EAST;
        } else if (horizontalInputValue < 0.0f) {
            return MoveDirection.WEST;
        }

        return MoveDirection.NONE;
    }
}
